use futures::{TryFuture, TryFutureExt};
use json_patch::{AddOperation, PatchOperation, RemoveOperation, TestOperation};
use k8s_openapi::Metadata;
use kube::{
    api::{ObjectMeta, Patch, PatchParams},
    Api,
};
use kube_runtime::controller::ReconcilerAction;
use serde::{de::DeserializeOwned, Serialize};
use snafu::{ResultExt, Snafu};
use std::error::Error as StdError;

#[derive(Debug, Snafu)]
pub enum Error<ApplyErr, CleanupErr>
where
    ApplyErr: StdError + 'static,
    CleanupErr: StdError + 'static,
{
    ApplyFailed { source: ApplyErr },
    CleanupFailed { source: CleanupErr },
    AddFinalizer { source: kube::Error },
    RemoveFinalizer { source: kube::Error },
}

pub async fn finalizer<K, ApplyFut, CleanupFut>(
    api: &Api<K>,
    finalizer: &str,
    obj: K,
    apply: impl FnOnce(K) -> ApplyFut,
    cleanup: impl FnOnce(K) -> CleanupFut,
) -> Result<ReconcilerAction, Error<ApplyFut::Error, CleanupFut::Error>>
where
    K: Metadata<Ty = ObjectMeta> + Clone + DeserializeOwned + Serialize,
    ApplyFut: TryFuture<Ok = ReconcilerAction>,
    ApplyFut::Error: StdError + 'static,
    CleanupFut: TryFuture<Ok = ()>,
    CleanupFut::Error: StdError + 'static,
{
    if let Some((finalizer_i, _)) = obj
        .metadata()
        .finalizers
        .as_ref()
        .and_then(|fins| fins.iter().enumerate().find(|(_, fin)| *fin == finalizer))
    {
        if obj.metadata().deletion_timestamp.is_none() {
            apply(obj).into_future().await.context(ApplyFailed)
        } else {
            let name = obj.metadata().name.clone().unwrap();
            cleanup(obj).into_future().await.context(CleanupFailed)?;
            let finalizer_path = format!("/metadata/finalizers/{}", finalizer_i);
            api.patch::<K>(
                &name,
                &PatchParams::default(),
                &Patch::Json(json_patch::Patch(vec![
                    PatchOperation::Test(TestOperation {
                        path: finalizer_path.clone(),
                        value: finalizer.into(),
                    }),
                    PatchOperation::Remove(RemoveOperation {
                        path: finalizer_path,
                    }),
                ])),
            )
            .await
            .context(RemoveFinalizer)?;
            Ok(ReconcilerAction {
                requeue_after: None,
            })
        }
    } else {
        if obj.metadata().deletion_timestamp.is_none() {
            let patch = json_patch::Patch(if obj.metadata().finalizers.is_none() {
                vec![
                    PatchOperation::Test(TestOperation {
                        path: "/metadata/finalizers".to_string(),
                        value: serde_json::Value::Null,
                    }),
                    PatchOperation::Add(AddOperation {
                        path: "/metadata/finalizers".to_string(),
                        value: vec![finalizer].into(),
                    }),
                ]
            } else {
                vec![PatchOperation::Add(AddOperation {
                    path: "/metadata/finalizers/-".to_string(),
                    value: finalizer.into(),
                })]
            });
            api.patch::<K>(
                obj.metadata().name.as_deref().unwrap(),
                &PatchParams::default(),
                &Patch::Json(patch),
            )
            .await
            .context(AddFinalizer)?;
        }
        Ok(ReconcilerAction {
            requeue_after: None,
        })
    }
}
